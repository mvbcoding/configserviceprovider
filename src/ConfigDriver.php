<?php

namespace MvbCoding\Silex;

interface ConfigDriver
{
    function load($filename);
    function supports($filename);
}
