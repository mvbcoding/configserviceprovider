# Change Log
All notable changes to this project will be documented in this file.
Items under `Unreleased` is upcoming features that will be out in next version.

This changelog follows the recommendations outlined at [keepachangelog.com](http://keepachangelog.com/).
It has links for the version diffs at the bottom.

This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [2.0.0] - 2016-06-07
### Added
- pimple3/silex2 support
### Removed
- silex1 support

## [1.0.0] - 2016-06-07
### Added
- Imported igorw's ConfigServiceProvider
### Changed
- Fixed yaml parsing with newer versions of symfony/yaml
- Switched to PSR-4
- Switched to MvbCoding namespace
