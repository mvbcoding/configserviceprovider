# ConfigServiceProvider

A config ServiceProvider for [Silex](http://silex.sensiolabs.org) with support
for php, json, yaml and toml.

Version 1.0.0 is an import of [igorw/ConfigServiceProvider](https://github.com/igorw/ConfigServiceProvider)
Since this repository seems dead, I took the liberty to create my own version.
All credits for this version go to [igorw](https://github.com/igorw). Thanks!

I also incorperated some fixes from outstanding PR's:

- [pr #37](https://github.com/igorw/ConfigServiceProvider/pull/37) by [Andreas Möller](https://github.com/localheinz)
- [pr #38](https://github.com/igorw/ConfigServiceProvider/pull/38) by [Parham Doustdar](https://github.com/parhamdoustdar)
- [pr #53](https://github.com/igorw/ConfigServiceProvider/pull/53) by [Aliaksandr Zakashanski](https://github.com/zak956)

## Usage

### Passing a config file

Pass the config file's path to the service provider's constructor. This is the
recommended way of doing it, allowing you to define multiple environments.

```php
$env = getenv('APP_ENV') ?: 'prod';
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/$env.json"));
```

Now you can specify a `prod` and a `dev` environment.

**config/prod.json**

```json
{
    "debug": false
}
```

**config/dev.json**

```json
{
    "debug": true
}
```

To switch between them, just set the `APP_ENV` environment variable. In apache
that would be:

```apache
SetEnv APP_ENV dev
```

Or in nginx with fcgi:

```nginx
fastcgi_param APP_ENV dev
```

Configuration values are set on the `$app` object. For example, to retrieve the `debug` configuration value, you can simply access the `debug` key of `$app`.

```php
$app->get('/', function() use ($app) {
    echo $app['debug'];
});
```

In order to prevent your configuration from overriding the keys that might be set on the application, you can pass in a `prefix` to the constructor. This prefix will then be available as a key under which all your other configuration can be accessed:

```php
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/$env.json", array(), null, 'example'));
```

You can now access the `debug` value defined in your configuration like this:

```php
$app->get('/', function() use ($app) {
    echo $app['example']['debug'];
});
```

### Replacements

Also, you can pass an array of replacement patterns as second argument.

```php
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/services.json", array(
    'data_path' => __DIR__.'/data',
)));
```

Now you can use the pattern in your configuration file.

**/config/services.json**

```json
{
    "xsl.path": "%data_path%/xsl"
}
```

You can also specify replacements inside the config file by using a key with
`%foo%` notation:

```json
{
    "%root_path%": "../..",
    "xsl.path": "%root_path%/xsl"
}
```

### Using Yaml

To use Yaml instead of JSON, just pass a file that ends on `.yml`:

```php
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/services.yml"));
```

Note, you will have to require the `^2.8|^3.0` of the `symfony/yaml` package.

### Using TOML

To use [TOML](https://github.com/mojombo/toml) instead of any of the other supported formats,
just pass a file that ends on `.toml`:

```php
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/services.toml"));
```

Note, you will have to require the `^1.1` of the `jamesmoss/toml` package and you are using
a bleeding edge configuration format, as the spec of TOML is still subject to change.

### Using plain PHP

If reading the config file on every request becomes a performance problem in
production, you can use a plain PHP file instead, and it will get cached by
APC.

You'll have to rewrite your config to be a PHP file that returns the array of
config data, and also make sure it ends with `.php`:

```php
$app->register(new MvbCoding\Silex\ConfigServiceProvider(__DIR__."/../config/prod.php"));
```

### Multiple config files

You can use multiple config files, e. g. one for a whole application and a
specific one for a task by calling `$app->register()` several times, each time
passing another instance of `MvbCoding\Silex\ConfigServiceProvider`.

### Register order

Make sure you register ConfigServiceProvider last with your application. If you do not do this,
the default values of other Providers will override your configuration.